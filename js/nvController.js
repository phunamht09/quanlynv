/**
Mô tả: Xây dựng lớp / prototype nhân viên
Người tạo: Phu Nam
Ngày tạo: 01/01/2023
Version: 1.0
 */

function layThongTinForm() {
        var _tkNV = document.getElementById("tknv").value;
        var _tenNV = document.getElementById("ten").value;
        var _emailNV = document.getElementById("email").value;
        var _passwordNV = document.getElementById("password").value;
        var _dateLV = document.getElementById("datepicker").value;
        var _luongCB = document.getElementById("luongCB").value * 1;
        var _chucVu = document.getElementById("chucvu").value;
        var _gioLam = document.getElementById("gioLam").value * 1;

        return new NhanVien(
            _tkNV,
            _tenNV,
            _emailNV,
            _passwordNV,
            _dateLV,
            _luongCB,
            _chucVu,
            _gioLam,
        );
}

function renderDSNV(nvArr) {
    var contentHTML = "";
    for (var index = 0; index < nvArr.length; index++) {
        var nv = nvArr[index];

        var contentTr = `<tr>
        <td>${nv.tkNV}</td>
        <td>${nv.tenNV}</td>
        <td>${nv.emailNV}</td>
        <td>${nv.dateLV}</td>
        <td>${nv.chucVu}</td>
        <td>${nv.tongLuong()}</td>
        <td>${nv.xepLoai()}</td>
        <td>
        <button class="btn btn-danger" onclick ="deleteItem(${nv.tkNV})">Xóa</button>
        <button class="btn btn-warning" onclick ="editItem(${nv.tkNV})">Sửa</button>
        </td>
        </tr>`;
        contentHTML = contentHTML + contentTr;
    }
    //show ra bang hien thi
    document.getElementById("tableDanhSach").innerHTML = contentHTML;
}
