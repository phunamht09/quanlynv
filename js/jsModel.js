// /**
// Mô tả: Xây dựng mảng nhân viên
// Người tạo: Phu Nam
// Ngày tạo: 01/01/2023
// Version: 1.0
//  */

//tao lop doi tuong
function NhanVien(
    _tkNV,
    _tenNV,
    _emailNV,
    _passwordNV,
    _dateLV,
    _luongCB,
    _chucVu,
    _gioLam,
) {
    this.tkNV = _tkNV;
    this.tenNV = _tenNV;
    this.emailNV = _emailNV;
    this.passwordNV = _passwordNV;
    this.dateLV = _dateLV;
    this.luongCB = _luongCB;
    this.chucVu = _chucVu;
    this.gioLam = _gioLam;
    this.tongLuong = function() {
        return (this.luongCB * 3).toLocaleString();
    };
    this.xepLoai = function() {
        var xepLoai = "";
        for (var index = 0; index < data.length; index++) {
            if (this.gioLam >= 192) {
                xepLoai = "Nhan vien xuat sac";
                return xepLoai;
            }     
        };
    };
};

